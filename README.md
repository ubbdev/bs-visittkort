# bs-visittkort #

Repo for [Visittkort Excel ark](https://prosjekt.uib.no/issues/6897) (prosjekt.uib.no)

> Helle har registrert en del visittkort og har skrevet metadataen inn i Excel. Hennes engasjement på BS er nå over men det ble allikevel en del poster. Legger med filen som dere kan flette inn i protege. Bildefilene skal være lastet opp.

Gjort med LodRefine 1.0.8.